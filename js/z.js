jQuery(window).load(function(){
    jQuery('#loading').fadeOut(3000);
});

$(document).ready(function () {

    var slideoutMenu = $('.slideout-menu');
    var slideoutMenuWidth = $('.slideout-menu').width();

    // Toggle navigation menu
    $('.slideout-menu-toggle').on('click', function(e){
        e.preventDefault();
        slideoutMenu.toggleClass("open");
        if (slideoutMenu.hasClass("open")) {
            slideoutMenu.animate({
                left: "0px"
            }); 
        } else {
            slideoutMenu.animate({
                left: -slideoutMenuWidth
            }, 250);    
        }
    });
    
    // Switch species
    $('.species-select').on('click', function(e){
        e.preventDefault();
        var id = this.id;
        slideoutMenu.toggleClass("open");
        slideoutMenu.animate({left: -slideoutMenuWidth}, 250);    
        $('#home-image').hide();
        $('.species').hide();
        $('#home').hide();
        $('#submission').hide();
        $('#submit-ty').hide();
        $('#species-'+id).show();
    });

    // Activate home page
    $('#home-select').on('click', function(e){
        e.preventDefault();
        slideoutMenu.toggleClass("open");
        slideoutMenu.animate({left: -slideoutMenuWidth}, 250);    
        $('#submission').hide();
        $('#submit-ty').hide();
        $('.species').hide();
        $('#home').show();
        $('#home-image').show();
    });

    // Activate submission page
    $('#submit-fish').on('click', function(e){
        e.preventDefault();
        slideoutMenu.toggleClass("open");
        slideoutMenu.animate({left: -slideoutMenuWidth}, 250);    
        $('.species').hide();
        $('#submit-ty').hide();
        $('#home').hide();
        $('#home-image').hide();
        $('#submission').show();
    });

    // Activate thank you page
    $('#submit-button').on('click', function(e){
        e.preventDefault();
        slideoutMenu.toggleClass("open");
        slideoutMenu.animate({left: -slideoutMenuWidth}, 250);    
        $('.species').hide();
        $('#submission').hide();
        $('#home').hide();
        $('#home-image').hide();
        $('#submit-ty').show();
    });

});
