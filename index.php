<?php

/******************************************************************************
 *
 *  Michigan Fishing
 *
 *  Michigan Fishing is a web based, mobile friendly application which allows
 *  users to interactively browse information about Michigan's native fish
 *  species, master anglers, and state fishing records.
 * 
 *  This application written for Code Michigan contest in Marquette, MI.
 *
 *  Author: Jason White <"JasonEdwardWhite@Gmail.com">
 *  Date:   20141004
 *
 * ***************************************************************************/



/******************************************************************************
 *
 *  Instantiate & configure Smarty template engine.
 *
 * ***************************************************************************/

require_once            ('./smarty/Smarty.class.php');
$smarty                 = new Smarty();
$smarty->setCaching     (Smarty::CACHING_LIFETIME_CURRENT);
$smarty->setTemplateDir ('./templates/');
$smarty->setCompileDir  ('./smarty/templates_c/');
$smarty->setConfigDir   ('./smarty/configs/');
$smarty->setCacheDir    ('./smarty/cache/');


/******************************************************************************
 *
 *  Soda-PHP is used to acquire Michigan fish, fish records and master anglers.
 *  
 *
 *  Soda-PHP Documentation
 *
 *    Querying
 *
 *    $socrata = new Socrata("http://data.michigan.gov");
 *    $response = $socrata->get("/resource/he9h-7fpa.json");
 *
 *    $socrata = new Socrata("https://data.michigan.gov", $app_token);
 *    $params = array("\$where" => "within_circle(location, $latitude, $longitude, $range)");
 *    $response = $socrata->get("/resource/$view_uid.json", $params);
 *
 *
 *    Publishing
 *
 *    $socrata = new Socrata("https://data.medicare.gov", $app_token, $user_name, $password);
 *  
 *      Publish data via 'upsert'
 *      $response = $socrata->post("/resource/abcd-2345.json", $data_as_json);
 *      
 *      Publish data via 'replace'
 *      $response = $socrata->put("/resource/abcd-2345.json", $data_as_json);
 *
 *    Display Media
 *
 *      https://data.michigan.gov/views/he9h-7fpa/files/<Image UUID>
 *
 * ***************************************************************************/

require_once            ('./inc/socrata.php');
$socrata                = new Socrata("http://data.michigan.gov");


/******************************************************************************
 *
 * Query and store our data sets using the Socrata API
 *
 * If I write this out to a file in the background each time, it will be
 * a mock-caching system and since this information is not time-critical,
 * I think I'll go ahead and do that because the dataset queries are slowing
 * down the page load speed considerably.  In the future it would make more
 * sense to load new data at a reasonable interval.
 *
 * ***************************************************************************/

// Query parameters
$param1 = array("\$order" => "year DESC");
$param2 = array("\$order" => "commonname ASC");
$param3 = array("\$order" => "species ASC");

// Queries
$anglers                = $socrata->get("/resource/mrpa-7cvr.json", $param1);
$fish                   = $socrata->get("/resource/he9h-7fpa.json", $param2);
$records                = $socrata->get("/resource/wi9z-3x3u.json", $param3);

// This would be a good place to normalize data since source data has issues


/******************************************************************************
 *
 *  Register Smarty objects
 *
 * ***************************************************************************/

$smarty->assign ('anglers', $anglers);
$smarty->assign ('fish',    $fish);
$smarty->assign ('records', $records);


/******************************************************************************
 *
 *  Start generating output via Smarty.
 *
 * ***************************************************************************/

$smarty->display ('head.tpl');
$smarty->display ('body.tpl');
$smarty->display ('foot.tpl');

?>
