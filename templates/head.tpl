<!DOCTYPE html>
<!--
Michigan Fishing app written by Jason White for Code Michigan 2014 in Marquette.
// -->
<!--[if lt IE 7 ]> <html class="ie ie6 no-js" lang="en" itemscope itemtype="http://schema.org/Blog"> <![endif]--><!--[if IE 7 ]>    <html class="ie ie7 no-js" lang="en" itemscope itemtype="http://schema.org/Blog"> <![endif]--><!--[if IE 8 ]>    <html class="ie ie8 no-js" lang="en" itemscope itemtype="http://schema.org/Blog"> <![endif]--><!--[if IE 9 ]>    <html class="ie ie9 no-js" lang="en" itemscope itemtype="http://schema.org/Blog"> <![endif]--><!--[if gt IE 9]><!-->
<html lang="en">
<!--<![endif]-->
    <head>
        <title>Michigan Fishing :: Master Anglers, Fish Records and Species</title>
        <meta http-equiv="Author" content="Jason Edward White - Marquette, MI">
        <meta http-equiv="Content-Language" content="EN">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="Copyright" content="Jason White, all rights reserved.  Copyright 2014.">
        <meta http-equiv="Description" content="Michigan Fishing is an application which interfaces with Michigan.gov data to display master angler records, state fish records and species related information.">
        <meta http-equiv="Distribution" content="Global">
        <meta http-equiv="Expires" content="">
        <meta http-equiv="Keywords" content="michigan, upper, lower, peninsula, local, regional, fish, records, species, master, angler, great, lakes">
        <meta http-equiv="Last-Modified" content="2014-10-04 08:59:16">
        <meta http-equiv="Revisit-After" content="7 Days">
        <meta http-equiv="Robots" content="index,follow">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="Author" content="Jason Edward White - Marquette, MI">
        <meta name="Copyright" content="Copyright Jason White 2012. All Rights Reserved.">
        <meta name="Viewport" content="width=device-width, initial-scale=1.0, target-densitydpi=device-dpi">
        <meta name="DC.title" content="Michigan Fishing :: Master Anglers, Fish Records and Species">
        <meta name="DC.subject" content="Michigan Fishing is an application which interfaces with Michigan.gov data to display master angler records, state fish records and species related information.">
        <meta name="DC.creator" content="Jason White">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="/favicon.ico">
        <link rel="shortcut icon" href="/favicon.ico">
        <link rel="apple-touch-icon" href="_/img/apple-touch-icon-114px.png">
        <link rel="stylesheet" href="http://fish.michigansocial.com/css/bootstrap.min.css" type="text/css" media="screen">
        <link rel="stylesheet" href="http://fish.michigansocial.com/css/bootstrap-theme.min.css" type="text/css" media="screen">
        <link rel="stylesheet" href="http://fish.michigansocial.com/css/jquery-ui.min.css" type="text/css" media="screen">
        <link rel="stylesheet" href="http://fish.michigansocial.com/css/jquery-ui-structure.min.css" type="text/css" media="screen">
        <link rel="stylesheet" href="http://fish.michigansocial.com/css/jquery-ui-theme.min.css" type="text/css" media="screen">
        <link rel="stylesheet" href="http://fish.michigansocial.com/css/site.css" type="text/css" media="screen">
        <meta http-equiv="Content-Script-Type" content="text/javascript">
        <script src="http://fish.michigansocial.com/js/jquery.js" type="text/javascript">
        </script>
        <script src="http://fish.michigansocial.com/js/jquery-ui.min.js" type="text/javascript">
        </script>
        <script src="http://fish.michigansocial.com/js/bootstrap.min.js" type="text/javascript">
        </script>
        <script src="http://fish.michigansocial.com/js/z.js" type="text/javascript">
        </script>
    </head>
    <body>

        <div id="header">

            <h1>
                Michigan Fishing
            </h1>
            <a href="#" class="slideout-menu-toggle" title="Show Menu">
                <i class="glyphicon glyphicon-list"></i> 
            </a>

        </div>

        <div id="home-image"></div>

        <div id="loading"></div>

        {include file="nav.tpl"}

        <div id="content">
