<div class="slideout-menu">

    <h3>Michigan Fish <a href="#" class="slideout-menu-toggle" title="Close Menu">×</a></h3>

    <ul>
        <li>
            <a href="javascript:void();" id="home-select">
                Home
                <i class="glyphicon glyphicon-chevron-right"></i>
            </a>
        </li>
        <li>
            <a href="javascript:void();" id="submit-fish">
                Submit Your Fish! 
                <i class="glyphicon glyphicon-chevron-right"></i>
            </a>
        </li>
        {foreach $fish as $record}
            {if !empty($record.commonname)}
                <li>
                    <a href="javascript:void();" class="species-select" id="{$record.id}">
                        {$record.commonname|replace:',':''}
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </a>
                </li>
            {elseif !empty($record.latinname)}
                <li>
                    <a href="javascript:void();" class="species-select" id="{$record.id}">
                        {$record.latinname}
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </a>
                </li>
            {/if}
        {/foreach}
    </ul>

</div>
