<div id="home">

    <h3>Learn about Michigan's fantastic fish, state records and master anglers!</h3>

</div>


<div id="submission">

    <h3>Submit Your Monster Fish!</h3>

    <form action="javascript:void();" id="submit-form">
        
        <label for="name">Full Name: </label>
        <input name="name" type="text"><br />
        <label for="city">Your City: </label>
        <input name="city" type="text"><br />
        <label for="species">Species Caught: </label>
        <input name="species" type="text"><br />
        <label for="length">Length in Inches: </label>
        <input name="length" type="text"><br />
        <label for="weight">Weight in Pounds: </label>
        <input name="weight" type="text"><br />
        <label for="lure">Bait / Lure: </label>
        <input name="lure" type="text"><br />
        <label for="method">Method: </label>
        <input name="method" type="text"><br />
        <label for="waterbody">Body of Water: </label>
        <input name="waterbody" type="text"><br />
        <label for="county">County Caught in: </label>
        <input name="county" type="text"><br />
        <label for="category">Category: </label>
        <input name="category" type="text"><br />
        <label for="year">Year Caught: </label>
        <input name="year" type="text"><br />
        <label for="date">Date Caught: </label>
        <input name="date" type="text"><br />
        <label for="picture">Picture: </label>
        <input name="date" type="file"><br />

        <button id="submit-button" type="submit">Send Fish!</button>

    </form><br />

</div>


<div id="submit-ty">

    <h3>Thank you for your submission!</h3>
    <p>Currently, your submission will not be forwarded to the Michigan Department of Natural
        resources for their consideration.  However, I will collect them and soon the front
        page will display a stream of user submitted fish!</p>
    <p>Tight lines till then,<br />
        Jason</p>

</div>


{foreach $fish as $record}
    {if !empty($record.commonname) || !empty($record.latinname)}

        <div class="species" id="species-{$record.id}">

            {* Species Data *}

                <div class="species-meta">
                    {if !empty($record.commonname)}
                        <div class="species-names">
                            <h2>{$record.commonname|capitalize:false:true|replace:',':''}</h2>
                            {if !empty($record.latinname)}
                                <p>({$record.latinname|capitalize:false:true})</p>
                            {/if}
                        </div>
                    {elseif !empty($record.latinname)}
                        <div class="species-names">
                            <h2>{$record.latinname|capitalize:false:true}</h2>
                        </div>
                    {/if}

                    {if !empty($record.image)}
                        <div class="species-image">
                            <img src="https://data.michigan.gov/views/he9h-7fpa/files/{$record.image}" alt="{$record.commonname}" title="{$record.commonname}" />
                        </div>
                    {/if}
                    {if !empty($record.narrative)}
                        <p class="species-narrative">{$record.narrative}</p>
                    {/if}
                </div>


            {* Record Holders *}

                {foreach $records as $r}
                    {if trim(strtolower($r.species)) == trim(strtolower($record.commonname))}

                        <hr />

                        <div id="records">

                            <h3>Current Michigan Record</h3>

                            <ul>
                                <li>Length: {$r.length|capitalize:false:true}"</li>
                                <li>Weight: {$r.weight} lbs</a></li>
                                <li>County: {$r.county|capitalize:false:true}</li>
                                <li>Waterbody: {$r.water_body|capitalize:false:true}</li>
                                <li>Bait/Lure: {$r.bait_lure|capitalize:false:true}</li>
                                <li>Method: {$r.method|capitalize:false:true}</li>
                                <li>Year: {$r.year}</li>
                            </ul>

                        </div>

                    {/if}
                {/foreach}


            {* Master Anglers *}

                {counter assign=c start=0}

                {foreach $anglers as $r}

                    {if trim(strtolower($r.species)) == trim(strtolower($record.commonname))}

                        {if (0 == $c)}
                            <div id="anglers">
                            <hr />
                            <h3>Master Anglers of {$record.commonname|capitalize:false:true|replace:',':''}</h3>
                            <ol>
                        {/if}

                        {counter}

                        {if !empty($r.angler)}
                            <li><strong>Angler: {$r.angler|capitalize:false:true}</strong></li>
                            <ul>
                                {if !empty($r.length_in)}
                                    <li>Length: {$r.length_in|capitalize:false:true}"</li>
                                {/if}
                                {if !empty($r.weight_lbs)}
                                    <li>Weight: {$r.weight_lbs|capitalize:false:true} lbs</li>
                                {/if}
                                {if !empty($r.bait)}
                                    <li>Bait: {$r.bait|capitalize:false:true}</li>
                                {/if}
                                {if !empty($r.method)}
                                    <li>Method: {$r.method|capitalize:false:true}</li>
                                {/if}
                                {if !empty($r.category)}
                                    <li>Category: {$r.category|capitalize:false:true}</li>
                                {/if}
                                {if !empty($r.county)}
                                    <li>County: {$r.county|capitalize:false:true}</li>
                                {/if}
                                {if !empty($r.waterbody)}
                                    <li>Waterbody: {$r.waterbody|capitalize:false:true}</li>
                                {/if}
                                {if !empty($r.year)}
                                    <li>Year: {$r.year}</li>
                                {/if}
                                {if !empty($r.angler_s_city)}
                                    <li>Angler's City: {$r.angler_s_city|capitalize:false:true}</li>
                                {/if}
                                {if !empty($r.date_time)}
                                    <li>Date &amp; Time: {$r.date_time}</li>
                                {/if}
                            </ul>
                        {else}
                            <p>No records found! Perhaps you would like to submit your own?</p>
                        {/if}

                    {/if}
                {/foreach}

                {if (0 < $c)}
                    </ol>
                    </div>
                {/if}


        </div>

    {/if}
{/foreach}
